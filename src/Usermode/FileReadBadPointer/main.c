#include "common_lib.h"
#include "syscall_if.h"
#include "um_lib_helper.h"

STATUS
__main(
    DWORD       argc,
    char**      argv
)
{
    STATUS status;
    UM_HANDLE handle;
    QWORD bytesRead;

    UNREFERENCED_PARAMETER(argc);
    UNREFERENCED_PARAMETER(argv);

    int suc = 1;

    status = SyscallFileCreate("HAL9000.ini",
                               sizeof("HAL9000.ini"),
                               FALSE,
                               FALSE,
                               &handle);
    if (!SUCCEEDED(status))
    {
        LOG_FUNC_ERROR("SyscallFileCreate", status);
        suc = 0;
    }

     status = SyscallFileRead(handle, (PVOID) 0x14'0000'0000ULL, PAGE_SIZE, &bytesRead);
     if (SUCCEEDED(status))
     {
         LOG_ERROR("SyscallFileRead should have failed!\n");
         suc = 0;
     }

     if (suc == 1)
     {
         LOG_TEST_PASS;
     }
    return STATUS_SUCCESS;
}