#include "common_lib.h"
#include "syscall_if.h"
#include "um_lib_helper.h"

STATUS
__main(
    DWORD       argc,
    char**      argv
)
{
    STATUS status;
    UM_HANDLE hProcess;
    STATUS termStatus;

    UNREFERENCED_PARAMETER(argc);
    UNREFERENCED_PARAMETER(argv);

    int suc = 1;
    
    __try
    {
        status = SyscallProcessCreate("dummy.exe",
                                      sizeof("dummy.exe"),
                                      NULL,
                                      0,
                                      &hProcess);
        if (!SUCCEEDED(status))
        {
            LOG_FUNC_ERROR("SyscallProcessCreate", status);
            suc = 0;
            __leave;
        }

        status = SyscallProcessWaitForTermination(hProcess, &termStatus);
        if (!SUCCEEDED(status))
        {
            LOG_FUNC_ERROR("SyscallThreadWaitForTermination", status);
            suc = 0;
            __leave;
        }

        status = SyscallProcessCloseHandle(hProcess);
        if (!SUCCEEDED(status))
        {
            LOG_FUNC_ERROR("SyscallProcessCloseHandle", status);
            suc = 0;
            __leave;
        }

        status = SyscallProcessCloseHandle(hProcess);
        if (SUCCEEDED(status))
        {
            LOG_ERROR("SyscallProcessCloseHandle should have failed on the second close attempt!\n");
            suc = 0;
            __leave;
        }
    }
    __finally
    {

    }

    if (suc == 1)
    {
        LOG_TEST_PASS;
    }

    return STATUS_SUCCESS;
}