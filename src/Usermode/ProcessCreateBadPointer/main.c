#include "common_lib.h"
#include "syscall_if.h"
#include "um_lib_helper.h"

STATUS
__main(
    DWORD       argc,
    char**      argv
)
{
    STATUS status;
    UM_HANDLE hProcessHandle;

    UNREFERENCED_PARAMETER(argc);
    UNREFERENCED_PARAMETER(argv);

    status = SyscallProcessCreate(NULL,
                                  0,
                                  NULL,
                                  0,
                                  &hProcessHandle);
    int suc = 1;
    if (SUCCEEDED(status))
    {
        LOG_ERROR("Process creation with invalid pointer should have failed!\n");
        suc = 0;
    }

    if (suc == 1)
    {
        LOG_TEST_PASS;
    }


    return STATUS_SUCCESS;
}