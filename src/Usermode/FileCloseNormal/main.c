#include "common_lib.h"
#include "syscall_if.h"
#include "um_lib_helper.h"

STATUS
__main(
    DWORD       argc,
    char**      argv
)
{
    UM_HANDLE handle;
    STATUS status;

    UNREFERENCED_PARAMETER(argc);
    UNREFERENCED_PARAMETER(argv);

    int suc = 1;
    __try
    {
        status = SyscallFileCreate("HAL9000.ini",
                                   sizeof("HAL9000.ini"),
                                   FALSE,
                                   FALSE,
                                   &handle);
        if (!SUCCEEDED(status))
        {
            LOG_FUNC_ERROR("SyscallFileCreate", status);
            suc = 0;
            __leave;
        }

        status = SyscallFileClose(handle);
        if (!SUCCEEDED(status))
        {
            LOG_FUNC_ERROR("SyscallFileClose", status);

            suc = 0;
            __leave;
        }
    }
    __finally
    {

    }

    if (suc == 1)
    {
        LOG_TEST_PASS;
    }

    return STATUS_SUCCESS;
}