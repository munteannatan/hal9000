#include "common_lib.h"
#include "syscall_if.h"
#include "um_lib_helper.h"

STATUS
__main(
    DWORD       argc,
    char**      argv
)
{
    STATUS status;
    UM_HANDLE hProcessHandle;
    STATUS terminationStatus;

    UNREFERENCED_PARAMETER(argc);
    UNREFERENCED_PARAMETER(argv);

    hProcessHandle = UM_INVALID_HANDLE_VALUE;
    int suc = 1;

    __try
    {
        status = SyscallProcessCreate("args.exe",
                                      sizeof("args.exe"),
                                      "Let's get some arguments printed!",
                                      sizeof("Let's get some arguments printed!"),
                                      &hProcessHandle);
        if (!SUCCEEDED(status))
        {
            LOG_FUNC_ERROR("SyscallProcessCreate", status);
            suc = 0;
        }

        status = SyscallProcessWaitForTermination(hProcessHandle, &terminationStatus);
        if (!SUCCEEDED(status))
        {
            LOG_FUNC_ERROR("SyscallProcessWaitForTermination", status);
            suc = 0;
        }
    }
    __finally
    {
        if (hProcessHandle != UM_INVALID_HANDLE_VALUE)
        {
            status = SyscallProcessCloseHandle(hProcessHandle);
            if (!SUCCEEDED(status))
            {
                LOG_FUNC_ERROR("SyscallProcessCloseHandle", status);
                suc = 0;
            }

            hProcessHandle = UM_INVALID_HANDLE_VALUE;
        }
    }

    if (suc == 1)
    {
        LOG_TEST_PASS;
    }

    return STATUS_SUCCESS;
}