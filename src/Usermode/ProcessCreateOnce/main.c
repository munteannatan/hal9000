#include "common_lib.h"
#include "syscall_if.h"
#include "um_lib_helper.h"

STATUS
__main(
    DWORD       argc,
    char**      argv
)
{
    STATUS status;
    UM_HANDLE hProcess;
    STATUS terminationStatus;

    UNREFERENCED_PARAMETER(argc);
    UNREFERENCED_PARAMETER(argv);

    hProcess = UM_INVALID_HANDLE_VALUE;
    int suc = 1;
    __try
    {
        status = SyscallProcessCreate("dummy.exe",
                                      sizeof("dummy.exe"),
                                      NULL,
                                      0,
                                      &hProcess);
        if (!SUCCEEDED(status))
        {
            LOG_FUNC_ERROR("SyscallProcessCreate", status);
            suc = 0;
            __leave;
        }

        if (hProcess == UM_INVALID_HANDLE_VALUE)
        {
            LOG_ERROR("0x%X is not a valid handle value for a process!\n", UM_INVALID_HANDLE_VALUE);
            suc = 0;
        }

        status = SyscallProcessWaitForTermination(hProcess, &terminationStatus);
        if (!SUCCEEDED(status))
        {
            LOG_FUNC_ERROR("SyscallProcessWaitForTermination", status);
            suc = 0;
        }
    }
    __finally
    {
        if (hProcess != UM_INVALID_HANDLE_VALUE)
        {
            status = SyscallProcessCloseHandle(hProcess);
            if (!SUCCEEDED(status))
            {
                LOG_FUNC_ERROR("SyscallProcessCloseHandle", status);
                suc = 0;
            }
            hProcess = UM_INVALID_HANDLE_VALUE;
        }
    }

    if (suc == 1)
    {
        LOG_TEST_PASS;
    }

    return STATUS_SUCCESS;
}