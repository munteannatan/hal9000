#include "common_lib.h"
#include "syscall_if.h"
#include "um_lib_helper.h"

STATUS
__main(
    DWORD       argc,
    char**      argv
)
{
    STATUS status;
    UM_HANDLE hProcess;

    UNREFERENCED_PARAMETER(argc);
    UNREFERENCED_PARAMETER(argv);

    status = SyscallProcessCreate("dummy.exe",
                                  sizeof("dummy.exe"),
                                  NULL,
                                  0,
                                  &hProcess);

    int succ = 1;

    if (!SUCCEEDED(status))
    {
        LOG_FUNC_ERROR("SyscallProcessCreate", status);
        succ = 0;
    }
    else
    {
        STATUS termStatus;
        

        status = SyscallProcessWaitForTermination(hProcess, &termStatus);
        if (!SUCCEEDED(status))
        {
            LOG_FUNC_ERROR("SyscallThreadWaitForTermination", status);
            succ = 0;
        }

        status = SyscallProcessCloseHandle(hProcess);
        if (!SUCCEEDED(status))
        {
            LOG_FUNC_ERROR("SyscallProcessCloseHandle", status);
            succ = 0;
        }
    }

    if (succ == 1)
        LOG_TEST_PASS;
    
    
    return STATUS_SUCCESS;
}