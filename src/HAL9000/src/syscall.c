#include "HAL9000.h"
#include "syscall.h"
#include "gdtmu.h"
#include "syscall_defs.h"
#include "syscall_func.h"
#include "syscall_no.h"
#include "mmu.h"
#include "process_internal.h"
#include "dmp_cpu.h"
#include "thread.h"
#include "thread_internal.h"
#include "iomu.h"
#include "io.h"
#include "vmm.h"

#define UM_HANDLE_INCREMENT               4

extern void SyscallEntry();

#define SYSCALL_IF_VERSION_KM       SYSCALL_IMPLEMENTED_IF_VERSION

static GLOBAL_FILE_LIST m_fileData;

PGLOBAL_FILE_LIST
GetOpenFileData()
{
    return &m_fileData;
}

STATUS
FindFileByHandle(
    IN      UM_HANDLE       Handle,
    IN      BOOLEAN         SearchForOpenFle,
    OUT     PFILE_OBJECT*   FileObject
)
{
    LIST_ITERATOR iterator;

    MutexAcquire(&m_fileData.FileListLock);

    ListIteratorInit(&m_fileData.FileList, &iterator);

    PLIST_ENTRY pEntry;

    while ((pEntry = ListIteratorNext(&iterator)) != NULL)
    {
        PFILE_OBJECT file = CONTAINING_RECORD(pEntry, FILE_OBJECT, NextFile);
        if (file != NULL)
        {
            //LOG("ALL files: handle:%x \n", file->Handle);
            if (SearchForOpenFle) 
            {
                if (file->Handle == Handle && file->Open)
                {
                    *FileObject = file;
                    MutexRelease(&m_fileData.FileListLock);
                    return STATUS_SUCCESS;
                }
            }
            else 
            {
                if (file->Handle == Handle)
                {
                    *FileObject = file;
                    MutexRelease(&m_fileData.FileListLock);
                    return STATUS_SUCCESS;
                }
            }
            
        }
    }
    *FileObject = NULL;
    MutexRelease(&m_fileData.FileListLock);
    return STATUS_ELEMENT_NOT_FOUND;
}

__forceinline
static
UM_HANDLE
_SyscallGetNextUmHandle(
    void
    )
{
    static volatile UM_HANDLE __currentUmHandle = 3;
    return _InterlockedExchangeAdd64(&__currentUmHandle, UM_HANDLE_INCREMENT);
}

STATUS
SyscallFindThreadByHandle(
    IN      UM_HANDLE       ThreadHandle,
    OUT     PTHREAD*        Thread
    )
{
    STATUS status = CL_STATUS_ELEMENT_NOT_FOUND;
    INTR_STATE oldState;
    PPROCESS process = GetCurrentProcess();

    LockAcquire(&process->ThreadListLock, &oldState);
    for (PLIST_ENTRY pEntry = process->ThreadList.Flink;
        pEntry != &process->ThreadList;
        pEntry = pEntry->Flink)
    {
        PTHREAD pThread = CONTAINING_RECORD(pEntry, THREAD, ProcessList);
        UM_HANDLE pThreadUmHandle = ThreadGetUmHandle(pThread);

        if ( (UM_INVALID_HANDLE_VALUE != pThreadUmHandle) && (pThreadUmHandle == ThreadHandle))
        {
            *Thread = pThread;
            status = STATUS_SUCCESS;
        }
    }
    LockRelease(&process->ThreadListLock, oldState);

    return status;
}

void
SyscallHandler(
    INOUT   COMPLETE_PROCESSOR_STATE    *CompleteProcessorState
    )
{
    SYSCALL_ID sysCallId;
    PQWORD pSyscallParameters;
    PQWORD pParameters;
    STATUS status;
    REGISTER_AREA* usermodeProcessorState;

    ASSERT(CompleteProcessorState != NULL);

    // It is NOT ok to setup the FMASK so that interrupts will be enabled when the system call occurs
    // The issue is that we'll have a user-mode stack and we wouldn't want to receive an interrupt on
    // that stack. This is why we only enable interrupts here.
    ASSERT(CpuIntrGetState() == INTR_OFF);
    CpuIntrSetState(INTR_ON);

    //LOG("The syscall handler has been called!\n");

    status = STATUS_SUCCESS;
    pSyscallParameters = NULL;
    pParameters = NULL;
    usermodeProcessorState = &CompleteProcessorState->RegisterArea;

    __try
    {
        if (LogIsComponentTraced(LogComponentUserMode))
        {
            DumpProcessorState(CompleteProcessorState);
        }

        // Check if indeed the shadow stack is valid (the shadow stack is mandatory)
        pParameters = (PQWORD)usermodeProcessorState->RegisterValues[RegisterRbp];
        status = MmuIsBufferValid(pParameters, SHADOW_STACK_SIZE, PAGE_RIGHTS_READ, GetCurrentProcess());
        if (!SUCCEEDED(status))
        {
            LOG_FUNC_ERROR("MmuIsBufferValid", status);
            __leave;
        }

        sysCallId = usermodeProcessorState->RegisterValues[RegisterR8];

        //LOG("System call ID is %u\n", sysCallId);

        // The first parameter is the system call ID, we don't care about it => +1
        pSyscallParameters = (PQWORD)usermodeProcessorState->RegisterValues[RegisterRbp] + 1;

        // Dispatch syscalls
        switch (sysCallId)
        {
        case SyscallIdIdentifyVersion:
            status = SyscallValidateInterface((SYSCALL_IF_VERSION)*pSyscallParameters);
            break;
        case SyscallIdThreadCreate:
            status = SyscallThreadCreate((PFUNC_ThreadStart)pSyscallParameters[0], (PVOID)pSyscallParameters[1], (UM_HANDLE*)pSyscallParameters[2]);
            break;
        case SyscallIdThreadExit:
            status = SyscallThreadExit((STATUS)pSyscallParameters[0]);
            break;
        case SyscallIdThreadGetTid:
            status = SyscallThreadGetTid((UM_HANDLE)pSyscallParameters[0], (TID*)pSyscallParameters[1]);
            break;
        case SyscallIdThreadWaitForTermination:
            status = SyscallThreadWaitForTermination((UM_HANDLE)pSyscallParameters[0], (STATUS*)pSyscallParameters[1]);
            break;
        case SyscallIdThreadCloseHandle:
            status = SyscallThreadCloseHandle((UM_HANDLE)pSyscallParameters[0]);
            break;
        case SyscallIdFileWrite:
            status = SyscallFileWrite((UM_HANDLE)pSyscallParameters[0], (PVOID)pSyscallParameters[1], (QWORD)pSyscallParameters[2], (QWORD*)pSyscallParameters[3]);
            break;

        case SyscallIdProcessExit:
            status = SyscallProcessExit((STATUS)pSyscallParameters[0]);
            break;

        case SyscallIdProcessGetPid:
            status = SyscallProcessGetPid( (UM_HANDLE) pSyscallParameters[0], (PID*) pSyscallParameters[1]);
            break;

        case SyscallIdProcessCreate:
            status = SyscallProcessCreate(
                        (char*) pSyscallParameters[0], 
                        (QWORD) pSyscallParameters[1],
                        (char*) pSyscallParameters[2], 
                        (QWORD) pSyscallParameters[3], 
                        (UM_HANDLE*) pSyscallParameters[4]);
            break;
        
        case SyscallIdProcessWaitForTermination:
            status = SyscallProcessWaitForTermination((UM_HANDLE) pSyscallParameters[0], (STATUS*) pSyscallParameters[1]);
            break;

        case SyscallIdProcessCloseHandle: 
            status = SyscallProcessCloseHandle( (UM_HANDLE) pSyscallParameters[0]);
            break;

        case SyscallIdFileCreate:
            status =  SyscallFileCreate(                 
                        (char*) pSyscallParameters[0],
                        (QWORD) pSyscallParameters[1],
                        (BOOLEAN) pSyscallParameters[2],
                        (BOOLEAN) pSyscallParameters[3],
                        (UM_HANDLE *) pSyscallParameters[4]
                    );
            break;

        case SyscallIdFileClose:
            status = SyscallFileClose( (UM_HANDLE) pSyscallParameters[0]);
            break;
        case SyscallIdFileRead:
            status = SyscallFileRead(
                        (UM_HANDLE) pSyscallParameters[0],
                        (PVOID) pSyscallParameters[1],
                        (QWORD) pSyscallParameters[2],
                        (QWORD*) pSyscallParameters[3]);
            break;
        case SyscallIdVirtualAlloc:
            status = SyscallVirtualAlloc(
                        (PVOID) pSyscallParameters[0], 
                        (QWORD) pSyscallParameters[1], 
                        (VMM_ALLOC_TYPE) pSyscallParameters[2],
                        (PAGE_RIGHTS) pSyscallParameters[3],
                        (UM_HANDLE) pSyscallParameters[4],
                        (QWORD) pSyscallParameters[5],
                        (PVOID*) pSyscallParameters[6]);
            break;
        case SyscallIdVirtualFree:
            status = SyscallVirtualFree(
                        (PVOID) pSyscallParameters[0],
                        (QWORD) pSyscallParameters[1],
                        (VMM_FREE_TYPE) pSyscallParameters[2]);
            break;

        // STUDENT TODO: implement the rest of the syscalls
        default:
            LOG_ERROR("Unimplemented syscall called from User-space!\n");
            status = STATUS_UNSUPPORTED;
            break;
        }

    }
    __finally
    {
        //LOG("Will set UM RAX to 0x%x\n", status);

        usermodeProcessorState->RegisterValues[RegisterRax] = status;

        CpuIntrSetState(INTR_OFF);
    }
}

void
SyscallPreinitSystem(
    void
    )
{
    memzero(&m_fileData, sizeof(GLOBAL_FILE_LIST));

    InitializeListHead(&m_fileData.FileList);
    MutexInit(&m_fileData.FileListLock, FALSE);

}

STATUS
SyscallInitSystem(
    void
    )
{

    /*PFILE_OBJECT file;
    STATUS status= IoCreateFile(&file, "C:\\STD_OUT", FALSE, TRUE, FALSE);
    LOG("JONATHAN STD_OUT create file status: %x \n", status);
    if (STATUS_SUCCESS == status)
    {

        file->Handle = UM_FILE_HANDLE_STDOUT;
        file->FileName = "STD_OUT";

        MutexAcquire(&m_fileData.FileListLock);
        InsertTailList(&m_fileData.FileList, &file->NextFile);
        MutexRelease(&m_fileData.FileListLock);
    }
 */
    return STATUS_SUCCESS;
}

STATUS
SyscallUninitSystem(
    void
    )
{
    return STATUS_SUCCESS;
}

void
SyscallCpuInit(
    void
    )
{
    IA32_STAR_MSR_DATA starMsr;
    WORD kmCsSelector;
    WORD umCsSelector;

    memzero(&starMsr, sizeof(IA32_STAR_MSR_DATA));

    kmCsSelector = GdtMuGetCS64Supervisor();
    ASSERT(kmCsSelector + 0x8 == GdtMuGetDS64Supervisor());

    umCsSelector = GdtMuGetCS32Usermode();
    /// DS64 is the same as DS32
    ASSERT(umCsSelector + 0x8 == GdtMuGetDS32Usermode());
    ASSERT(umCsSelector + 0x10 == GdtMuGetCS64Usermode());

    // Syscall RIP <- IA32_LSTAR
    __writemsr(IA32_LSTAR, (QWORD) SyscallEntry);

    LOG_TRACE_USERMODE("Successfully set LSTAR to 0x%X\n", (QWORD) SyscallEntry);

    // Syscall RFLAGS <- RFLAGS & ~(IA32_FMASK)
    __writemsr(IA32_FMASK, RFLAGS_INTERRUPT_FLAG_BIT);

    LOG_TRACE_USERMODE("Successfully set FMASK to 0x%X\n", RFLAGS_INTERRUPT_FLAG_BIT);

    // Syscall CS.Sel <- IA32_STAR[47:32] & 0xFFFC
    // Syscall DS.Sel <- (IA32_STAR[47:32] + 0x8) & 0xFFFC
    starMsr.SyscallCsDs = kmCsSelector;

    // Sysret CS.Sel <- (IA32_STAR[63:48] + 0x10) & 0xFFFC
    // Sysret DS.Sel <- (IA32_STAR[63:48] + 0x8) & 0xFFFC
    starMsr.SysretCsDs = umCsSelector;

    __writemsr(IA32_STAR, starMsr.Raw);

    LOG_TRACE_USERMODE("Successfully set STAR to 0x%X\n", starMsr.Raw);
}

// SyscallIdIdentifyVersion
STATUS
SyscallValidateInterface(
    IN  SYSCALL_IF_VERSION          InterfaceVersion
)
{
    LOG_TRACE_USERMODE("Will check interface version 0x%x from UM against 0x%x from KM\n",
        InterfaceVersion, SYSCALL_IF_VERSION_KM);

    if (InterfaceVersion != SYSCALL_IF_VERSION_KM)
    {
        LOG_ERROR("Usermode interface 0x%x incompatible with KM!\n", InterfaceVersion);
        return STATUS_INCOMPATIBLE_INTERFACE;
    }

    return STATUS_SUCCESS;
}

// SyscallIdThreadCreate
STATUS
SyscallThreadCreate(
    IN      PFUNC_ThreadStart       StartFunction,
    IN_OPT  PVOID                   Context,
    OUT     UM_HANDLE*              ThreadHandle
    )
{
    STATUS status;
    PTHREAD pThread;
    PPROCESS process = GetCurrentProcess();

    status = MmuUMIsParameterValid((PVOID)StartFunction, sizeof(PFUNC_ThreadStart), PAGE_RIGHTS_READ, process);
    if (!SUCCEEDED(status)) 
    {
        return status;
    }

    status = ThreadCreateEx("User thread", ThreadPriorityDefault, StartFunction, Context, &pThread, process);
    if (!SUCCEEDED(status)) 
    {
        return status;
    }
   
    pThread->UmHandle = _SyscallGetNextUmHandle();
    *ThreadHandle = pThread->UmHandle;

    return status;
}

// SyscallIdThreadExit
STATUS
SyscallThreadExit(
    IN  STATUS                      ExitStatus
    )
{
    ThreadExit(ExitStatus);
    return STATUS_SUCCESS;
}

// SyscallIdThreadGetTid
STATUS
SyscallThreadGetTid(
    IN_OPT  UM_HANDLE               ThreadHandle,
    OUT     TID*                    ThreadId
    )
{
    if (NULL == ThreadId)
    {
        return STATUS_INVALID_BUFFER;
    }

    STATUS status = STATUS_SUCCESS;
    PTHREAD pThread;
    
    if (UM_INVALID_HANDLE_VALUE != ThreadHandle) 
    {
        status = SyscallFindThreadByHandle(ThreadHandle, &pThread);
        if (!SUCCEEDED(status))
        {
            return status;
        }
    }
    else 
    {
        pThread = NULL;
    }

    *ThreadId = ThreadGetId(pThread);

    return status;
}

// SyscallIdThreadWaitForTermination
STATUS
SyscallThreadWaitForTermination(
    IN      UM_HANDLE               ThreadHandle,
    OUT     STATUS*                 TerminationStatus
    )
{
    if (NULL == TerminationStatus)
    {
        return STATUS_INVALID_BUFFER;
    }

    if (UM_INVALID_HANDLE_VALUE == ThreadHandle)
    {
        return STATUS_INVALID_PARAMETER1;
    }

    STATUS status;
    PTHREAD pThread;

    status = SyscallFindThreadByHandle(ThreadHandle, &pThread);
    if (!SUCCEEDED(status))
    {
        return status;
    }

    ThreadWaitForTermination(pThread, TerminationStatus);

    return status;
}

// SyscallIdThreadCloseHandle
STATUS
SyscallThreadCloseHandle(
    IN      UM_HANDLE               ThreadHandle
    )
{
    if (UM_INVALID_HANDLE_VALUE == ThreadHandle)
    {
        return STATUS_INVALID_PARAMETER1;
    }

    STATUS status;
    PTHREAD pThread;

    status = SyscallFindThreadByHandle(ThreadHandle, &pThread);
    if (!SUCCEEDED(status))
    {
        return status;
    }

    ThreadCloseHandle(pThread);
    pThread->UmHandle = UM_INVALID_HANDLE_VALUE;

    return status;
}

STATUS
SyscallFileWrite(
    IN  UM_HANDLE                   FileHandle,
    IN_READS_BYTES(BytesToWrite)
    PVOID                           Buffer,
    IN  QWORD                       BytesToWrite,
    OUT QWORD*                      BytesWritten
    )
{
    STATUS status;

    if (UM_FILE_HANDLE_STDOUT != FileHandle)
    {
        return STATUS_DEVICE_INVALID_OPERATION;
    }

    status = MmuUMIsParameterValid(Buffer, SHADOW_STACK_SIZE, PAGE_RIGHTS_WRITE, GetCurrentProcess());
    if (!SUCCEEDED(status))
    {
        LOG_FUNC_ERROR("MmuUMIsParameterValid", status);
        return status;
    }

    *BytesWritten = BytesToWrite;

    LOG("[%s]:[%s]\n", ProcessGetName(NULL), Buffer);

    return status;
}

STATUS
SyscallProcessExit(
    IN      STATUS                  ExitStatus
)
{
    PPROCESS proc = GetCurrentProcess();

    ProcessTerminate(proc, ExitStatus);

    return STATUS_SUCCESS;
}

STATUS GetAbosultePath(IN char* GivenPath, IN QWORD   PathLength, OUT char* PathToFile, IN BOOLEAN IsNotExe)
{
    
    STATUS status;

    if (IsNotExe)
    {
        if (GivenPath[1] == ':' && GivenPath[2] == '\\')
        {
            //abolut:
            status = sprintf(PathToFile, GivenPath);
            //LOG("\tABEL, filePath absolut: %s  , partition:%s\n", PathToFile, IomuGetSystemPartitionPath());
        }
        else
        {
            //relativ to system drive
            status = sprintf(PathToFile, "%s%s", IomuGetSystemPartitionPath(), GivenPath);
            //LOG("\tABEL , filePath with sys partition added : %s  , partition:%s\n", PathToFile, IomuGetSystemPartitionPath());
        }
    }
    else
    {
        if (PathLength <= 4) // must be at least *.exe
        {
            //LOG("\tABEL, path error, too short %s\n", GivenPath);
            return STATUS_INVALID_FILE_NAME;
        }

        if (GivenPath[1] == ':' && GivenPath[2] == '\\')
        {
            // absolute path 
            status = sprintf(PathToFile, GivenPath);
            //LOG("\tABEL, processPath absolut: %s  , partition:%s\n", PathToFile, IomuGetSystemPartitionPath());
        }
        else
        {
            //relative path 
            status = sprintf(PathToFile, "%sApplications\\%s", IomuGetSystemPartitionPath(), GivenPath);
            //LOG("\tABEL , processPath with sys partition added : %s  , partition:%s\n", PathToFile, IomuGetSystemPartitionPath());
        }
    }
    

    return STATUS_SUCCESS;
}

STATUS
SyscallProcessCreate(
    IN_READS_Z(PathLength)
    char* ProcessPath,
    IN          QWORD               PathLength,
    IN_READS_OPT_Z(ArgLength)
    char* Arguments,
    IN          QWORD               ArgLength,
    OUT         UM_HANDLE* ProcessHandle
)
{
    PPROCESS proc = GetCurrentProcess();

    STATUS status = MmuUMIsParameterValid((PVOID)ProcessPath, PathLength, PAGE_RIGHTS_READ, proc);

    if (STATUS_SUCCESS != status)
    {
        *ProcessHandle = UM_INVALID_HANDLE_VALUE;
        return status;
    }

    if (NULL != Arguments)
    {
        status = MmuUMIsParameterValid((PVOID)Arguments, ArgLength, PAGE_RIGHTS_READ, proc);

        if (STATUS_SUCCESS != status)
        {
            *ProcessHandle = UM_INVALID_HANDLE_VALUE;
            return status;
        }
    }

    char* PathToExe = NULL;
    DWORD  len = (DWORD)PathLength + 12 + 3 + 1;
    PathToExe = ExAllocatePoolWithTag(PoolAllocateZeroMemory, len, HEAP_PROCESS_TAG, 0);
    if (PathToExe == NULL)
    {
        *ProcessHandle = UM_INVALID_HANDLE_VALUE;
        return STATUS_INTERNAL_ERROR;
    }


    if(STATUS_SUCCESS != GetAbosultePath(ProcessPath, PathLength, PathToExe, FALSE))
    {
        *ProcessHandle = UM_INVALID_HANDLE_VALUE;
        LOG("\tABEL , UM_INVALID_HANDLE_VALUE status:%d %s\n", status, PathToExe);

        ExFreePoolWithTag((PVOID)PathToExe, HEAP_PROCESS_TAG);
        return status;
    }
        
    UM_HANDLE handle = UM_INVALID_HANDLE_VALUE;

    PPROCESS process;

    status = ProcessCreate(PathToExe, Arguments, &process);

    if (STATUS_SUCCESS != status)
    {
        handle = UM_INVALID_HANDLE_VALUE;   
        LOG("\tABEL , UM_INVALID_HANDLE_VALUE\n");
    }
    else
    {
        handle = _SyscallGetNextUmHandle();
        LOG("\tABEL , handle:%X\n", handle);
        if (process != NULL)
        {
            process->Handle = handle;
            LOG("\tABEL ,  process not NULL\n");
        }
    }

    LOG("\tABEL processcreate status %d\n", status);
   
  
    *ProcessHandle = handle;
    ExFreePoolWithTag((PVOID)PathToExe, HEAP_PROCESS_TAG);

    return status;
}


STATUS
SyscallProcessGetPid(
    IN_OPT  UM_HANDLE   ProcessHandle,
    OUT     PID* ProcessId
)
{

    if (UM_INVALID_HANDLE_VALUE == ProcessHandle)
    {
        *ProcessId = 0;
        return STATUS_SUCCESS;
    }

    PPROCESS proc;
    STATUS status = FindProcessByHandle(ProcessHandle, &proc);
    if (STATUS_SUCCESS == status)
    {
        if (proc != NULL)
        {
            *ProcessId = proc->Id;        
        }
        else
        {
            *ProcessId = 0;
        }
        return STATUS_SUCCESS;
    }

    *ProcessId = 0;
    return STATUS_SUCCESS;
}

STATUS
SyscallProcessWaitForTermination(
    IN      UM_HANDLE               ProcessHandle,
    OUT     STATUS* TerminationStatus
)
{
    if (TerminationStatus == NULL)
    {
        LOG("\tABEL ,  HERE 0\n");
        return STATUS_INVALID_BUFFER;
    }

    LOG("\tABEL ,  HERE 1\n");

    if (UM_INVALID_HANDLE_VALUE == ProcessHandle)
    {
        *TerminationStatus = STATUS_UNSUCCESSFUL;
        return STATUS_SUCCESS;
    }
    LOG("\tABEL ,  HERE 2\n");
    PPROCESS proc;
    STATUS status = FindProcessByHandle(ProcessHandle, &proc);
    if ((STATUS_SUCCESS != status) || (proc == NULL))
    {
        LOG("\tABEL ,  HERE 3\n");
        *TerminationStatus = STATUS_UNSUCCESSFUL;
        return status;   
    }

    LOG("\tABEL ,  HERE 4\n");
    STATUS termistatus;

    ProcessWaitForTermination(proc, &termistatus);
   
    LOG("\tABEL ,  HERE 9 %x\n", termistatus);
    *TerminationStatus = termistatus;

    return STATUS_SUCCESS;
}


STATUS
SyscallProcessCloseHandle(
    IN      UM_HANDLE               ProcessHandle
)
{
    if (UM_INVALID_HANDLE_VALUE == ProcessHandle)
    {
        return STATUS_INVALID_PARAMETER1;
    }

    PPROCESS cur = GetCurrentProcess();
    PPROCESS parent = NULL;
    if(cur != NULL)
        parent = (PPROCESS)cur->ParentProcess;

    if (parent != NULL)
    {
        if (parent->Handle == ProcessHandle)
        {
            LOG("\tABEL can't kill parent");
            return STATUS_INVALID_PARAMETER1;
        }
    }

    PPROCESS proc;
    STATUS status = FindProcessByHandle(ProcessHandle, &proc);
    if ((STATUS_SUCCESS != status) || (proc == NULL))
    {
        LOG("\tABEL SyscallProcessCloseHandle not found\n");
        return status;
    }

    ProcessCloseHandle(proc);
    LOG("\tABEL SyscallProcessCloseHandle success\n");
    return STATUS_SUCCESS;
}


STATUS
SyscallFileCreate(
    IN_READS_Z(PathLength)
    char* Path,
    IN          QWORD                   PathLength,
    IN          BOOLEAN                 Directory,
    IN          BOOLEAN                 Create,
    OUT         UM_HANDLE* FileHandle
)
{

    if (PathLength == 0)
    {
        *FileHandle = UM_INVALID_HANDLE_VALUE;
        return STATUS_INVALID_PARAMETER2;
    }

    PPROCESS proc = GetCurrentProcess();
    STATUS status = MmuUMIsParameterValid((PVOID)Path, PathLength, PAGE_RIGHTS_READ, proc);

    if (STATUS_SUCCESS != status)
    {
        *FileHandle = UM_INVALID_HANDLE_VALUE;
        return status;
    }

    if (proc->OpenFilesCount >= PROCESS_MAX_OPEN_FILES)
    {
        *FileHandle = UM_INVALID_HANDLE_VALUE;
        return STATUS_UNSUCCESSFUL;
    }

    char* PathToFile = NULL;
    PathToFile = ExAllocatePoolWithTag(PoolAllocateZeroMemory, MAX_PATH, HEAP_PROCESS_TAG, 0);
    if (PathToFile == NULL)
    {
        *FileHandle = UM_INVALID_HANDLE_VALUE;
        return STATUS_INTERNAL_ERROR;
    }


    if (STATUS_SUCCESS != GetAbosultePath(Path, PathLength, PathToFile, TRUE))
    {
        *FileHandle = UM_INVALID_HANDLE_VALUE;
        LOG("\tABEL , UM_INVALID_HANDLE_VALUE status:%d %s\n", status, PathToFile);

        ExFreePoolWithTag((PVOID)PathToFile, HEAP_PROCESS_TAG);
        return STATUS_INTERNAL_ERROR;
    }


    PFILE_OBJECT file;

    status = IoCreateFile(&file, PathToFile, Directory, Create, FALSE);

    if (status == STATUS_SUCCESS)
    {
        if (file != NULL)
        {
            file->Handle = _SyscallGetNextUmHandle();

            MutexAcquire(&m_fileData.FileListLock);
            InsertTailList(&m_fileData.FileList, &file->NextFile);
            MutexRelease(&m_fileData.FileListLock);

            file->ParentProcess = (PVOID)proc;
            *FileHandle = file->Handle;

            proc->OpenFilesCount += 1;
        }
        else
        {
            *FileHandle = UM_INVALID_HANDLE_VALUE;
        }
    }
    else
    {
        *FileHandle = UM_INVALID_HANDLE_VALUE;
    }
    
    ExFreePoolWithTag((PVOID)PathToFile, HEAP_PROCESS_TAG);
    return status;
}


STATUS
SyscallFileClose(
    IN          UM_HANDLE               FileHandle
)
{
    if (UM_INVALID_HANDLE_VALUE == FileHandle)
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if (UM_FILE_HANDLE_STDOUT == FileHandle)
    {
        PPROCESS proc = GetCurrentProcess();
        if (proc->StdOutHandleClosed == TRUE)
            return STATUS_UNSUCCESSFUL;

        proc->StdOutHandleClosed = TRUE;
        return STATUS_SUCCESS;
    }


    PFILE_OBJECT file;
    STATUS status = FindFileByHandle(FileHandle, TRUE, &file);

    if ((STATUS_SUCCESS != status) || (file == NULL))
    {
        //LOG("\tABEL SyscallFileClose not found\n");
        return status;
    }

    status = IoCloseFile(file);

    if (status == STATUS_SUCCESS)
    {
        MutexAcquire(&m_fileData.FileListLock);

        PPROCESS proc = (PPROCESS)file->ParentProcess;
        proc->OpenFilesCount -= 1;

        RemoveEntryList(&file->NextFile);

        file->ParentProcess = NULL;

        MutexRelease(&m_fileData.FileListLock);
    }

    //LOG("\tABEL SyscallFileClose status: %X\n", status);
    return status;

}

STATUS
SyscallFileRead(
    IN  UM_HANDLE                   FileHandle,
    OUT_WRITES_BYTES(BytesToRead)
    PVOID                       Buffer,
    IN  QWORD                       BytesToRead,
    OUT QWORD* BytesRead
)
{
    if (UM_INVALID_HANDLE_VALUE == FileHandle)
    {
        *BytesRead = 0;
        return STATUS_INVALID_PARAMETER1;
    }

    if (BytesToRead == 0)
    {
        *BytesRead = 0;
        Buffer = NULL;
        return STATUS_SUCCESS;
    }

    PPROCESS proc = GetCurrentProcess();
    STATUS status = MmuUMIsParameterValid((PVOID)Buffer, BytesToRead, PAGE_RIGHTS_WRITE, proc);

    if (STATUS_SUCCESS != status)
    {
        *BytesRead = 0;
        return STATUS_INVALID_PARAMETER2;
    }


    PFILE_OBJECT file;
    status = FindFileByHandle(FileHandle, TRUE, &file);

    if ((STATUS_SUCCESS != status) || (file == NULL))
    {
        LOG("\tABEL SyscallFileRead handle not found\n");
        *BytesRead = 0;
        return STATUS_NO_DATA_AVAILABLE;
    }

    status = IoReadFile(file, BytesToRead, NULL, Buffer, BytesRead);

    return status;
}

// SyscallIdVirtualAlloc
STATUS
SyscallVirtualAlloc(
    IN_OPT      PVOID                   BaseAddress,
    IN          QWORD                   Size,
    IN          VMM_ALLOC_TYPE          AllocType,
    IN          PAGE_RIGHTS             PageRights,
    IN_OPT      UM_HANDLE               FileHandle,
    IN_OPT      QWORD                   Key,
    OUT         PVOID*                  AllocatedAddress
    )
{
    UNREFERENCED_PARAMETER(Key);

    STATUS status = STATUS_SUCCESS;
    PPROCESS currentProcess;
    PFILE_OBJECT fileObject;

    if (VmIsKernelAddress(BaseAddress))
    {
        return STATUS_INVALID_PARAMETER1;
    }
    if (PAGE_RIGHTS_ALL == PageRights || (PAGE_RIGHTS_WRITE | PAGE_RIGHTS_EXECUTE) == PageRights)
    {
        return STATUS_INVALID_PARAMETER2;
    }
    if (Size < 1)
    {
        return STATUS_INVALID_PARAMETER3;
    }
    if (UM_INVALID_HANDLE_VALUE != FileHandle)
    {
        status = FindFileByHandle(FileHandle, FALSE, &fileObject);
        if ((STATUS_SUCCESS != status) || (fileObject == NULL))
        {
            return status;
        }
    }
    else
    {
        //acest else e mai mult pentru a teste pentru ca in mod normal daca UM_HANDLE este invalid se opreste apelul de sistem
        fileObject = (PFILE_OBJECT)FileHandle;
    }

    currentProcess = GetCurrentProcess();
    *AllocatedAddress = VmmAllocRegionEx(BaseAddress, Size, AllocType, PageRights, TRUE, fileObject, currentProcess->VaSpace, currentProcess->PagingData, NULL);
    return status;
}

// SyscallIdVirtualFree
STATUS
SyscallVirtualFree(
    IN          PVOID                   Address,
    _When_(VMM_FREE_TYPE_RELEASE == FreeType, _Reserved_)
    _When_(VMM_FREE_TYPE_RELEASE != FreeType, IN)
    QWORD                               Size, 
    IN          VMM_FREE_TYPE           FreeType
    )
{
    PPROCESS currentProcess;

    if (VmIsKernelAddress(Address))
    {
        return STATUS_INVALID_PARAMETER1;
    }

    currentProcess = GetCurrentProcess();
    VmmFreeRegionEx(Address, Size, FreeType, TRUE,currentProcess->VaSpace, currentProcess->PagingData);
    return STATUS_SUCCESS;
} 