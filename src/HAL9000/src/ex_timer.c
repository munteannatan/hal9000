#include "HAL9000.h"
#include "ex_timer.h"
#include "iomu.h"
#include "thread_internal.h"

typedef struct _GLOBAL_TIMER_LIST
{
	// protect the global timer list
	LOCK TimerListLock;
	// the list 's head
	_Guarded_by_(TimerListLock)
		LIST_ENTRY TimerListHead;
}GLOBAL_TIMER_LIST, * PGLOBAL_TIMER_LIST;

static GLOBAL_TIMER_LIST m_globalTimerList;

STATUS
ExTimerInit(
    OUT     PEX_TIMER       Timer,
    IN      EX_TIMER_TYPE   Type,
    IN      QWORD           Time
    )
{
    STATUS status;

    if (NULL == Timer)
    {
        return STATUS_INVALID_PARAMETER1;
    }

    if (Type > ExTimerTypeMax)
    {
        return STATUS_INVALID_PARAMETER2;
    }

    status = STATUS_SUCCESS;

    memzero(Timer, sizeof(EX_TIMER));

    Timer->Type = Type;
    if (Timer->Type != ExTimerTypeAbsolute)
    {
        // relative time

        // if the time trigger time has already passed the timer will
        // be signaled after the first scheduler tick
        Timer->TriggerTimeUs = IomuGetSystemTimeUs() + Time;
        Timer->ReloadTimeUs = Time;
    }
    else
    {
        // absolute
        Timer->TriggerTimeUs = Time;
    }

	status = ExEventInit(&Timer->TimerEvent, ExEventTypeNotification, FALSE);
	if (status != STATUS_SUCCESS)
	{
		return status;
	}

	INTR_STATE dummyState;

	LockAcquire(&m_globalTimerList.TimerListLock, &dummyState);
	InsertOrderedList(&m_globalTimerList.TimerListHead, &Timer->TimerListElem, ExTimerCompareListElems, NULL);
	LockRelease(&m_globalTimerList.TimerListLock, dummyState);

    return status;
}

void
ExTimerStart(
    IN      PEX_TIMER       Timer
    )
{
    ASSERT(Timer != NULL);

    if (Timer->TimerUninited)
    {
        return;
    }

    Timer->TimerStarted = TRUE;
}

void
ExTimerStop(
    IN      PEX_TIMER       Timer
    )
{
    ASSERT(Timer != NULL);

    if (Timer->TimerUninited)
    {
        return;
    }

    Timer->TimerStarted = FALSE;
	ExEventSignal(&Timer->TimerEvent);
}

void
ExTimerWait(
    INOUT   PEX_TIMER       Timer
    )
{
    ASSERT(Timer != NULL);

    if (Timer->TimerUninited)
    {
        return;
    }

    /*while (IomuGetSystemTimeUs() < Timer->TriggerTimeUs && Timer->TimerStarted)
    {
        ThreadYield();
    }*/

	ExEventWaitForSignal(&Timer->TimerEvent);
}

void
ExTimerUninit(
    INOUT   PEX_TIMER       Timer
    )
{
    ASSERT(Timer != NULL);

    ExTimerStop(Timer);

    Timer->TimerUninited = TRUE;

	INTR_STATE dummyState;
	LockAcquire(&m_globalTimerList.TimerListLock, &dummyState);
	RemoveEntryList(&Timer->TimerListElem);
	LockRelease(&m_globalTimerList.TimerListLock, dummyState);
}

INT64
ExTimerCompareTimers(
    IN      PEX_TIMER     FirstElem,
    IN      PEX_TIMER     SecondElem
)
{
    return FirstElem->TriggerTimeUs - SecondElem->TriggerTimeUs;
}

void
ExTimerSystemPreinit(
	void
)
{
	memzero(&m_globalTimerList, sizeof(struct _GLOBAL_TIMER_LIST));
	LockInit(&m_globalTimerList.TimerListLock);
	InitializeListHead(&m_globalTimerList.TimerListHead);
}



INT64
ExTimerCompareListElems(
	IN PLIST_ENTRY t1,
	IN PLIST_ENTRY t2,
	IN PVOID context
)
{
	ASSERT(t1 != NULL);
	ASSERT(t2 != NULL);
	(context);

	PEX_TIMER pTimer1 = CONTAINING_RECORD(t1, EX_TIMER, TimerListElem);
	PEX_TIMER pTimer2 = CONTAINING_RECORD(t2, EX_TIMER, TimerListElem);

	return ExTimerCompareTimers(pTimer1, pTimer2);
}



void
ExTimerCheck(
	IN PEX_TIMER timer
)
{
	ExEventSignal(&timer->TimerEvent);
}

STATUS ExTimerCheckListFunction(
	IN        PLIST_ENTRY     ListEntry,
	IN_OPT 	  PVOID           FunctionContext)
{
	(FunctionContext);
	STATUS status = STATUS_SUCCESS;
	PEX_TIMER pTimer = CONTAINING_RECORD(ListEntry, EX_TIMER, TimerListElem);
	

	if (IomuGetSystemTimeUs() < pTimer->TriggerTimeUs)
	{
		status = STATUS_ELEMENT_FOUND;
	}
	else
	{
		ExTimerCheck(pTimer);
	}

	return status;
}

void
ExTimerCheckAll(
	void
)
{
	INTR_STATE dummyState;
	LockAcquire(&m_globalTimerList.TimerListLock, &dummyState);
	
	ForEachElementExecute(&m_globalTimerList.TimerListHead, ExTimerCheckListFunction, NULL, FALSE);

	LockRelease(&m_globalTimerList.TimerListLock, dummyState);
}
