#pragma once
#include "mutex.h"
#include "list.h"

typedef struct _GLOBAL_FILE_LIST
{
    LIST_ENTRY FileList;

    MUTEX FileListLock;

} GLOBAL_FILE_LIST, * PGLOBAL_FILE_LIST;


void
SyscallPreinitSystem(
    void
    );

STATUS
SyscallInitSystem(
    void
    );

STATUS
SyscallUninitSystem(
    void
    );

void
SyscallCpuInit(
    void
    );


PGLOBAL_FILE_LIST
GetOpenFileData(
    void
);

